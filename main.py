import csv
import numpy as np
from keras.models import Model
from keras.models import Sequential
from keras.layers import Embedding, LSTM, Dense, Input, Concatenate, Reshape
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.src.callbacks import EarlyStopping
from sklearn.model_selection import train_test_split


def define_model(total_words_text, max_sequence_length_text, total_words_name):
    input_text = Input(shape=(max_sequence_length_text,))
    embedding_text = Embedding(input_dim=total_words_text, output_dim=100,
                               input_length=max_sequence_length_text)(
        input_text)

    lstm_text = LSTM(100, return_sequences=True)(embedding_text)
    lstm_text = LSTM(50)(lstm_text)

    model_name = Sequential()
    model_name.add(Embedding(input_dim=total_words_name, output_dim=100, input_length=1))
    model_name.add(Reshape((100,)))

    merged = Concatenate()([lstm_text, model_name.output])
    dense = Dense(16)(merged)

    output = Dense(len(set(names)), activation='softmax')(dense)

    model = Model(inputs=[input_text, model_name.input], outputs=output)

    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

    return model


# Read data from CSV
data = []
with open("dataset.csv", 'r') as csv_file:
    csv_reader = csv.reader(csv_file)
    for row in csv_reader:
        name = row[0] if len(row) > 0 else ''
        text = row[1] if len(row) > 1 else ''
        data_dict = {"name": name, "text": text}
        data.append(data_dict)

names = [item["name"] for item in data]
texts = [item["text"] for item in data]

# Tokenize text and names
tokenizer_text = Tokenizer()
tokenizer_text.fit_on_texts(texts)
total_words_text = len(tokenizer_text.word_index) + 1

tokenizer_name = Tokenizer()
tokenizer_name.fit_on_texts(names)
total_words_name = len(tokenizer_name.word_index) + 1

# Create input sequences for text and names
input_sequences_text = tokenizer_text.texts_to_sequences(texts)
max_sequence_length_text = max(len(seq) for seq in input_sequences_text)
padded_sequences_text = pad_sequences(input_sequences_text, maxlen=max_sequence_length_text, padding='post')
input_sequences_name = tokenizer_name.texts_to_sequences(names)
padded_sequences_name = pad_sequences(input_sequences_name, padding='post')

# Convert names to numerical labels
name_labels = {"lucy": 0, "charlie": 1, "unknown": 2}
numeric_labels = [name_labels[name] for name in names]

# Split the data into training and testing sets
train_text, test_text, train_name, test_name, train_labels, test_labels = train_test_split(
    padded_sequences_text, padded_sequences_name, numeric_labels, test_size=0.2, random_state=42
)

# Define the model
model = define_model(total_words_text, max_sequence_length_text, total_words_name)

# Definizione del callback EarlyStopping
early_stopping = EarlyStopping(monitor='loss',  # Monitora la perdita sul set di dati di validazione
                               patience=3,  # Numero di epoche senza miglioramenti prima dell'arresto
                               restore_best_weights=True)  # Ripristina i pesi del modello alla migliore epoca

# Train the model
model.fit([train_text, train_name], np.array(train_labels), epochs=50, validation_split=0.2, callbacks=[early_stopping])

# Evaluate the model on the test data
test_loss, test_accuracy = model.evaluate([test_text, test_name], np.array(test_labels), verbose=0)
print(f'Model Accuracy: {test_accuracy * 100}%')

model.save('output_model.keras')


from keras.models import load_model

# Load the model
loaded_model = load_model('output_model.keras')

# Test the model
test_data = ["ciao, che fai? ", "i want each player to do twenty pushups everyday!", "hello, my friend!"]
test_names = ["unknown", "charlie", "unknown"]

test_sequences_text = tokenizer_text.texts_to_sequences(test_data)
padded_test_sequences_text = pad_sequences(test_sequences_text, maxlen=max_sequence_length_text, padding='post')
test_sequences_name = tokenizer_name.texts_to_sequences(test_names)
padded_test_sequences_name = pad_sequences(test_sequences_name, padding='post')

predictions = loaded_model.predict([padded_test_sequences_text, padded_test_sequences_name])

# Map predictions back to names
predicted_names = [list(name_labels.keys())[idx] for idx in predictions.argmax(axis=1)]

# Print the results and accuracies
for text, true_name, predicted_name, x in zip(test_data, test_names, predicted_names, range(3)):
    accuracies = [true_char == predicted_char for true_char, predicted_char in zip(true_name, predicted_name)]
    accuracy_percentage = sum(accuracies) / len(true_name) * 100
    print(
        f"Text: {text}, True Name: {true_name}, Predicted Name: {predicted_name}")
    # Print probabilities with the correct name
    # Use the corresponding name from test_names
    current_name = test_names[x]
    percentages = [round(prob * 100, 2) for prob in predictions[x]]
    print(f"Probability lucy: {percentages[0]}%")
    print(f"Probability charlie: {percentages[1]}%")
    print(f"Probability unknown: {percentages[2]}%")
